#include"tabuSearcher.hpp"

#include<iostream>
#include<exception>

void printHelp(std::string progName);

int main(int argc, char *argv[]){
    using namespace std;

    // only one parameter allowd
    if(argc < 2){
        printHelp(argv[0]);
        return 0;
    }

    // construct graph
    TabuSearcher searcher;
    TabuSearcher::Graph graph;
    try{
        graph = searcher.parseInputFile(argv[1]);
    } catch (logic_error e){
        cerr << e.what() << endl;
        return 1;
    }

    int startColorCount = 0;
    if(argc >= 3){
        try{
            startColorCount = stoi(argv[2]);
        } catch (exception) {
            printHelp(argv[0]);
            return 1;
        }
    }

    cout << searcher.startSearch(graph, startColorCount);
    return 0;
}

void printHelp(std::string progName){
    using namespace std;
    cout << "Usage " << progName << " <filename> [<start-color-count>]" << endl;
}
