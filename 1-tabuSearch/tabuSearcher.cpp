#include"tabuSearcher.hpp"
#include<iostream>
#include<fstream>
#include<sstream>
#include<iomanip>
#include<exception>

#include<cstdlib>
#include<ctime>
#include<climits>

TabuSearcher::Graph TabuSearcher::parseInputFile(const char *fileName){
#define MAX_LINE_SIZE 500
    using namespace std;

    fstream inFile;
    char buffer[MAX_LINE_SIZE];
    inFile.open(fileName, ios::in);

    if(!inFile.good())
        throw logic_error("Error opening file");

    // skip all comments
    do{
        buffer[0] = '\0';
        inFile.getline(buffer, MAX_LINE_SIZE, '\n');
    } while (buffer[0] == 'c');

    // read vex num and edge num
    if(buffer[0] != 'p'){
        cerr << "Illegal input file, 'p' should be right after 'c' section" << endl;
        inFile.close();
        throw logic_error("Illegal input file");
    }
    string tempString;
    int vexNum, edgeNum;
    stringstream(buffer) >> tempString >> tempString >> vexNum >> edgeNum;
    cout << vexNum  << " vexs, " << edgeNum << " edges" << endl;

    // initialize graph data
    Graph graph;
    graph = vector<vector<int>>(vexNum, vector<int>());

    // read and construct graph
    char tempChar;
    int startVex, endVex;
    for(int i=0; i < edgeNum; ++i){
        inFile >> tempChar >> startVex >> endVex;
        // conver 1-based array to 0-based array
        graph[startVex-1].push_back(endVex-1);
        graph[endVex-1].push_back(startVex-1);
    }
    if(! inFile.good()){
        inFile.close();
        throw logic_error("Illegal input file, line num and actual line num don't match");
    }

    inFile.close();
    return graph;
#undef MAX_LINE_SIZE
}

int TabuSearcher::startSearch(const Graph &graph, int StartColorCount){
    using namespace std;

#ifdef NDEBUG
    srand(time(0));
#else
    srand(0);
#endif

    int colorCount;
    if(StartColorCount == 0)
        colorCount = graph.size() + 1;
    else
        colorCount = StartColorCount + 1;

    // top-level countdown
    clock_t startTime = clock(), lastTime = startTime, thisTime;
    cout << "       found   iteration       timeStamp       roundTime" << endl;

    while(colorCount--){
        // create a search session
        SearchSession session(graph);

        // inialize the search session
        session.initialize(colorCount);

        // start search
        session.searchForOneColor(0);

        thisTime = clock();
        cout << setw(12) << colorCount
            << setw(12) << session.currentTime
            << setw(16) << std::fixed << static_cast<double>(thisTime - startTime)/CLOCKS_PER_SEC
            << setw(16) << std::fixed << static_cast<double>(thisTime-lastTime)/CLOCKS_PER_SEC << endl;
        lastTime = thisTime;
    } // colorcount--
    return 0;
}

int TabuSearcher::SearchSession::searchForOneColor(const int maxSteps){
    int iterCount = 0;
    while(score != 0){
        // find the best move
        Move bestMove = findMove();

        // break if cannot find a move
        if(!bestMove.isAvailable){
            return -1;
        }

        // make the best move
        makeMove(bestMove);

        ++iterCount;
        if(maxSteps != 0 && iterCount > maxSteps)
            return score;
    }
    return 0;
}

TabuSearcher::SearchSession::SearchSession(const Graph &_graph) : graph(_graph) {

}

void TabuSearcher::SearchSession::initialize(int _colorCount){
    using std::vector;

    // initialize colorCount
    colorCount = _colorCount;

    // initialize graph with random number
    vexColor = vector<VexColor>(graph.size());
    for(auto &color : vexColor)
        color = rand() % colorCount;

    // initialize neighborSet, score and historyBestScore
    score = 0;
    vexIsolate = vector<bool>(graph.size(), true);
    for(unsigned i=0; i < graph.size(); ++i)
        for(auto adjVex : graph[i])
            if(vexColor[i] == vexColor[adjVex]){
                vexIsolate[i] = false;
                ++score;
            }

    score /= 2;
    historyBestScore = score;

    // initialize adjColorTable
    adjColorTable = vector<vector<int>>(graph.size(), vector<int>(colorCount, 0));
    for(unsigned i=0; i < graph.size(); ++i)
        for(auto vex : graph[i])
            ++adjColorTable[i][vexColor[vex]];

    // initialize tabu turne
    tabuTenure = vector<vector<int>>(graph.size(), vector<int>(colorCount, 0));

    // initialize current time
    currentTime = 0;
}

void TabuSearcher::SearchSession::initializeWithColor(int _colorCount, const std::vector<VexColor> &_vexColor){
    using std::vector;

    // initialize colorCount
    colorCount = _colorCount;

    // initialize graph with random number
    vexColor = _vexColor;

    // initialize neighborSet, score and historyBestScore
    score = 0;
    vexIsolate = vector<bool>(graph.size(), true);
    for(unsigned i=0; i < graph.size(); ++i)
        for(auto adjVex : graph[i])
            if(vexColor[i] == vexColor[adjVex]){
                vexIsolate[i] = false;
                ++score;
            }

    score /= 2;
    historyBestScore = score;

    // initialize adjColorTable
    adjColorTable = vector<vector<int>>(graph.size(), vector<int>(colorCount, 0));
    for(unsigned i=0; i < graph.size(); ++i)
        for(auto vex : graph[i])
            ++adjColorTable[i][vexColor[vex]];

    // initialize tabu turne
    tabuTenure = vector<vector<int>>(graph.size(), vector<int>(colorCount, 0));

    // initialize current time
    currentTime = 0;
}

TabuSearcher::Move TabuSearcher::SearchSession::findMove(){
    /// @brief the equal color occurrance count of common move and tabu move
    int commonEqualCounter = 0, tabuEqualCounter = 0;

    /// @brief tabu move and non-tabu move
    Move tabuMove, commonMove;

    // calculate moves for all vex in neighbor set
    for(unsigned vex = 0; vex < graph.size() ; ++vex){
        if(vexIsolate[vex])
            continue;
        // traverse all dstColors available
        for(VexColor dstColor = 0; dstColor < colorCount; ++dstColor){
            // only calculate if this is not the same dstColor with the original dstColor
            if(dstColor != vexColor[vex]){
                // calcualte the delta if this move is performed
                int tempDelta = adjColorTable[vex][dstColor] - adjColorTable[vex][vexColor[vex]];
                // decide if this is a tabu move
                if(tabuTenure[vex][dstColor] <= currentTime){
                    if(tempDelta < commonMove.delta){
                        // if is best move and is not in tabu list
                        commonMove.delta = tempDelta;
                        commonMove.vex = vex;
                        commonMove.dstColor =  dstColor;
                        commonMove.isAvailable = true;
                        commonEqualCounter = 0;
                    } else if(tempDelta == commonMove.delta){
                        if(commonEqualCounter == 0 || (rand() % commonEqualCounter) == 0){
                            commonMove.delta = tempDelta;
                            commonMove.vex = vex;
                            commonMove.dstColor =  dstColor;
                            commonMove.isAvailable = true;
                        }
                        ++commonEqualCounter;
                    }
                } else {
                    if (tempDelta < tabuMove.delta){
                        // if is best move and is in tabu list
                        tabuMove.delta = tempDelta;
                        tabuMove.vex = vex;
                        tabuMove.dstColor = dstColor;
                        tabuMove.isAvailable = true;
                        tabuEqualCounter = 0;
                    } else if(tempDelta == tabuMove.delta){
                        if(tabuEqualCounter == 0 || (rand() % tabuEqualCounter) == 0){
                            tabuMove.delta = tempDelta;
                            tabuMove.vex = vex;
                            tabuMove.dstColor = dstColor;
                            tabuMove.isAvailable = true;
                        }
                        ++tabuEqualCounter;
                    }
                }
            }
        }
    }
    // decide the best move
    if(tabuMove.isAvailable &&
            tabuMove.delta < commonMove.delta &&
            score + tabuMove.delta < historyBestScore){
        // use tabumove in strict condition
        return tabuMove;
    } else if(commonMove.isAvailable){
        // use common move
        return commonMove;
    } else {
        // return a null move
        return Move();
    }
}

void TabuSearcher::SearchSession::makeMove(const Move &finalMove){
    //>> update adjColorTable
    for(auto adjVex : graph[finalMove.vex]){
        --adjColorTable[adjVex][vexColor[finalMove.vex]];
        ++adjColorTable[adjVex][finalMove.dstColor];
    }

    //>> set tabu time
    tabuTenure[finalMove.vex][vexColor[finalMove.vex]] = currentTime + score + (rand() % 10);

    //>> change vex color
    vexColor[finalMove.vex] = finalMove.dstColor;

    //>> update vex isolation status
    vexIsolate[finalMove.vex] = (adjColorTable[finalMove.vex][vexColor[finalMove.vex]] == 0);
    for(auto adjVex : graph[finalMove.vex])
        vexIsolate[adjVex] = (adjColorTable[adjVex][vexColor[adjVex]] == 0);

    //>> update score
    score += finalMove.delta;
    if(score < historyBestScore)
        historyBestScore = score;

    // update time
    ++currentTime;
}

TabuSearcher::SearchSession &TabuSearcher::SearchSession::operator=(const SearchSession &a){
    if(&graph != &(a.graph))
        throw std::logic_error("can't copy searchsession based on different graph");

    vexColor = a.vexColor;
    vexIsolate = a.vexIsolate;
    adjColorTable = a.adjColorTable;
    tabuTenure = a.tabuTenure;

    currentTime = a.currentTime;
    colorCount = a.colorCount;
    score = a.score;
    historyBestScore = a.historyBestScore;

    return *this;
}
