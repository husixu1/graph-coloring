#ifndef TABUSEARCHER_HPP_
#define TABUSEARCHER_HPP_

#include<iostream>
#include<vector>
#include<array>
#include<set>
#include<climits>

/// @brief the tabu searcher
/// Each tabu seacher searches for only one graph,
/// but it can do multiple searches simultaneously.
class TabuSearcher {
protected:
    typedef int VexIndex;                           ///< @brief use int as vex index
    typedef int VexColor;                           ///< @brief use int to identify color
    typedef int Time;                               ///< @brief use int to count time
    typedef int Score;                              ///< @brief use int to count score

public:
    typedef std::vector<std::vector<VexIndex>> Graph;       ///< @brief the graph to search

protected:

    /// @brief the definition of a move
    struct Move {
        Score delta;                                ///< @brief score delta if this move is made
        VexIndex vex;                               ///< @brief the vex to move
        VexColor dstColor;                          ///< @brief the destination color of this move
        bool isAvailable;                           ///< @brief is this move available

        /// @brief constructor
        Move() : delta(INT_MAX), vex(-1), dstColor(-1), isAvailable(false) {}
        Move(const Move &) = default;
        Move &operator= (const Move &) = default;
    };

    /// @brief each SearchSession stands for a search process
    /// SearchSession stores states of a search process.
    /// You can start multiple session on same graph without a conflict.
    /// One SearchSession only searchs on one graph for one color,
    /// and the graph cannot be changed once initialized.
    struct SearchSession{
        std::vector<VexColor> vexColor;             ///< @brief the vex's color
        std::vector<bool> vexIsolate;               ///< @brief the conflicting color set
        std::vector<std::vector<int>> adjColorTable;///< @brief adjacent color table
        std::vector<std::vector<Time>> tabuTenure;  ///< @brief tabu tenure (record the tabu step)

        Time currentTime;                           ///< @brief current time
        Score score;                                ///< @brief current score
        Score historyBestScore;                     ///< @brief the history best score
        int colorCount;                             ///< @brief the maximum color count of current search
        const Graph &graph;                         ///< @brief the graph to search

        /// @brief constructor
        /// @param graph the graph to search
        SearchSession(const Graph &graph);

        /// @brief initialize search-related datas for this session
        /// this function can be called multiple times. on second or later calls,
        /// the searchsession will be reset and re-initialized
        /// @param colorCount the color count of this session
        void initialize(int colorCount);

        /// @brief initialize search-related datas with pre-defined colors for each vex
        /// caller should gurantee colorCount matchs vexColor
        /// @param colorCount the color count of this session
        /// @param vexColor each vex's color
        void initializeWithColor(int colorCount, const std::vector<VexColor> &vexColor);

        /// @brief find the current best move
        /// @return the current best move
        Move findMove();

        /// @brief make a move on a session
        /// @param move the move to make
        void makeMove(const Move &move);

        /// @brief search for certain steps
        /// @param[in] maxSteps maximum step to iterate, 0 for unlimited
        /// @return final score (conflicts), 0 for found, -1 for no moves available
        int searchForOneColor(const int maxSteps);

        /// @brief copy the search session
        /// @param a the other search session to copy
        /// @exception logic_error if two session's graph is different
        /// @return current search session
        SearchSession &operator=(const SearchSession &a);
    };

public:
    /// @brief parse input file
    /// @param fileName the name of the input file
    /// @return the parse result. The size will be zero if failed
    static Graph parseInputFile(const char *fileName);

    /// @brief start searching for minimum color
    /// @param graph the Graph to search on
    /// @param StartColorCount the start searching color count
    /// @return the minimum color count found
    virtual int startSearch(const Graph &graph, int StartColorCount = 0);

    /// @brief constructor
    TabuSearcher() = default;
    /// @brief destroyer
    ~TabuSearcher() = default;

    /// @brief copy-constructor
    TabuSearcher(const TabuSearcher &) = delete;
    /// @brief copy function
    TabuSearcher &operator=(const TabuSearcher &) = delete;
};

#endif // TABUSEARCHER_HPP_
