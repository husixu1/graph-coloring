#!/bin/bash

set -x
CURDIR="$( cd "$( dirname "${BASH_SOURCE[0]}"  )" && pwd  )"
LOGDIR=${CURDIR}/log

COMMIT="$(git log -n 1 --pretty=oneline |  grep -E '^[[:alnum:]]+' -o)"

if ! [[ $1 ]]; then
    echo "Usage $0 <input-file> [<other-args>]"
    exit 0
fi

LOGFILE="${LOGDIR}/$(date +%F_%T)_$(basename "$1").log"

if ! [[ -d LOGDIR  ]]; then
    mkdir -p "${LOGDIR}" || exit 1
fi

echo "commit: ${COMMIT}" >  "${LOGFILE}"
echo "starttime: $(date +%F_%T)" >> "${LOGFILE}"

if [[ $2 ]]; then
    ./tabuSearch "$1" "$2" | tee -a "${LOGFILE}"
else
    ./tabuSearch "$1" | tee -a "${LOGFILE}"
fi
