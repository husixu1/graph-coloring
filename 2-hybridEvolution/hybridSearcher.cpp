#include"hybridSearcher.hpp"
#include<iostream>
#include<iomanip>
#include<vector>
#include<functional>

#include<cstdlib>
#include<climits>

int HybridSearcher::startSearch(
        const Graph &graph,
        int maxSteps,
        int startColorCount,
        const int populationSize){
    using namespace std;

    if(populationSize < 2)
        throw logic_error("population size must be greater than 2");

#ifdef NDEBUG
    srand(time(0));
#else
    srand(0);
#endif

    // initialize colorcount
    int colorCount;
    if(startColorCount == 0)
        colorCount = graph.size() + 1;
    else
        colorCount = startColorCount + 1;

    // initialize output
    clock_t startTime = clock(), lastTime = startTime, thisTime;
    cout << "       found   crossover     iterate       timeStamp       roundTime" << endl;

    // top-level countdown
    while(--colorCount){
        int crossoverCount = 0, iterateCount = 0;

        // initialize population
        std::vector<SearchSession> population;
        for(int i=0; i < populationSize; ++i){
            SearchSession temp = SearchSession(graph);
            temp.initialize(colorCount);
            population.push_back(temp);
        }

        // initial calculate
        int result;
        bool solutionFound = false;
        for(int i=0; i < populationSize; ++i){
            result = population[i].searchForOneColor(maxSteps);
            iterateCount += population[i].currentTime;
            if(result <= 0){
                solutionFound = true;
                break;
            }
        }
        if(solutionFound){
            thisTime = clock();
            cout << setw(12) << colorCount
                << setw(12) << crossoverCount
                << setw(12) << iterateCount
                << setw(16) << std::fixed << static_cast<double>(thisTime - startTime)/CLOCKS_PER_SEC
                << setw(16) << std::fixed << static_cast<double>(thisTime - lastTime)/CLOCKS_PER_SEC << endl;
            lastTime = thisTime;
            continue;
        }

        // get best solution
        int lowestScore = INT_MAX, lowestIndex, highestScore = INT_MIN, highestIndex;
        SearchSession bestSolution(graph), worstSolution(graph);
        for(int i=0; i < populationSize; ++i){
            if(population[i].score < lowestScore){
                lowestScore = population[i].score;
                lowestIndex = i;
            }
            if(population[i].score >= highestScore){
                highestScore = population[i].score;
                highestIndex = i;
            }
        }

        bestSolution = population[lowestIndex];
        worstSolution = population[highestIndex];

        SearchSession temp(graph);
        while(lowestScore != 0){
            int indexA = rand() % populationSize, indexB = rand() % populationSize;
            while(indexB == indexA)
                indexB = rand() % populationSize;

            // do crossover
            temp.initializeWithColor(
                    colorCount,
                    crossoverOperator(population[indexA].vexColor, population[indexB].vexColor, colorCount));
            ++crossoverCount;

            // search for the crossover result
            result = temp.searchForOneColor(maxSteps);
            iterateCount += temp.currentTime;
            if(result <= 0)
                break;

//cout << colorCount << endl;
//for(unsigned i=0; i<temp.vexColor.size(); ++i)
//    cout << temp.vexColor[i];
//cout << endl;

            // update best solution
            if(temp.score < bestSolution.score){
                lowestScore = temp.score;
                bestSolution = temp;
            }

            // update pool and worst solution
            population[highestIndex] = temp;
            highestScore = INT_MIN, highestIndex = -1;
            for(int i=0; i < populationSize; ++i)
                if(population[i].score >= highestScore){
                    highestScore = population[i].score;
                    highestIndex = i;
                }
            worstSolution = population[highestIndex];
        }

        thisTime = clock();
        cout << setw(12) << colorCount
            << setw(12) << crossoverCount
            << setw(12) << iterateCount
            << setw(16) << std::fixed << static_cast<double>(thisTime - startTime)/CLOCKS_PER_SEC
            << setw(16) << std::fixed << static_cast<double>(thisTime - lastTime)/CLOCKS_PER_SEC << endl;
        lastTime = thisTime;
    } // --colorcount
    return 0;
}

std::vector<int> HybridSearcher::crossoverOperator(
        std::vector<int> a,
        std::vector<int> b,
        const int colorCount){
    using std::vector;

    // record each color's number
    vector<int> colorNums(colorCount, 0);
    int maxColorNum, maxColor;
    vector<int> ans(a.size(), -1);

    for(int assignColor=0; assignColor < colorCount; ++assignColor){
        vector<int> *mainVexColor = assignColor % 2 ? &b : &a;
        vector<int> *subVexColor = assignColor % 2 ? &a : &b;

        // count each color's number
        colorNums = vector<int>(colorCount, 0);
        for(auto color : (*mainVexColor)){
            if(color != -1)
                ++colorNums[color];
        }

        // decide the max color
        maxColorNum = 0, maxColor = -1;
        for(int i=0; i < colorCount; ++i)
            if(colorNums[i] > maxColorNum){
                maxColorNum = colorNums[i];
                maxColor = i;
            }

        // do the crossover
        for(unsigned i=0; i < a.size(); ++i){
            if((*mainVexColor)[i] == maxColor){
                (*mainVexColor)[i] = -1;
                (*subVexColor)[i] = -1;
                ans[i] = assignColor;
            }
        }
    }

    // randomly assign the leftovers
    for(unsigned i=0; i < a.size(); ++i)
        if(a[i] != -1)
            ans[i] = rand() % colorCount;

    return ans;
}
