#ifndef HYBRIDSEARCHER_HPP_
#define HYBRIDSEARCHER_HPP_

#include"../1-tabuSearch/tabuSearcher.hpp"
#include<vector>
#include<queue>

class HybridSearcher : public TabuSearcher{
protected:
    std::vector<std::vector<int>> bestSolutions;

    std::vector<int> crossoverOperator(
            std::vector<int> a,
            std::vector<int> b,
            const int colorCount);

public:
    /// @brief start searching for minimum color
    /// @param graph the Graph to search on
    /// @param startColorCount the start searching color count
    /// @param population the population of this search
    /// @return the minimum color count found
    virtual int startSearch(
            const Graph &graph,
            int maxSteps = 100000,
            int startColorCount = 0,
            const int population = 2);
};

#endif // HYBRIDSEARCHER_HPP_
