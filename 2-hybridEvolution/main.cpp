#include"hybridSearcher.hpp"
#include<iostream>
#include<vector>
#include<string>

void printHelp(std::string progName);
int main(int argc, char *argv[]){
    using namespace std;

    // only one parameter allowd
    if(argc < 2){
        printHelp(argv[0]);
        return 0;
    }

    int maxIterStep = 10000, startColorCount = 0, populationSize = 2;
    try{
        if(argc >= 3)
            maxIterStep = stoi(argv[2]);
        if(argc >= 4)
            startColorCount = stoi(argv[3]);
        if(argc >= 5)
            populationSize = stoi(argv[4]);
    } catch (exception){
        printHelp(argv[0]);
        return 1;
    }

    // construct graph
    HybridSearcher searcher;
    HybridSearcher::Graph graph = searcher.parseInputFile(argv[1]);

    cout << searcher.startSearch(graph, maxIterStep, startColorCount, populationSize);
    return 0;
}

void printHelp(std::string progName){
    using namespace std;
    cout << "Usage: " << progName << " <filename> [<max-iterate-step> [<start-color-count> [<population-size>]]]" << endl
        << "    if not specified, <max-iter-step> is defaulted to 100000" << endl
        << "    if not specified, <start-color-count> is defaulted to vex number" << endl
        << "    if not specifiec, <population-size> is defaulted to 2" << endl;
}
